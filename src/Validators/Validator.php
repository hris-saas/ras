<?php

namespace HRis\RAS\Validators;

class Validator
{
    private static $validators = [];

    public static function registerValidators(): void
    {
        foreach (self::$validators as $validator) {
            (new $validator())->handle();
        }
    }
}
