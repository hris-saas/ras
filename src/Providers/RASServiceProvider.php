<?php

namespace HRis\RAS\Providers;

use HRis\RAS\Validators\Validator;
use HRis\Core\Providers\BaseServiceProvider;

class RASServiceProvider extends BaseServiceProvider
{
    /**
     * Perform post-registration booting of services.
     *
     * @return void
     */
    public function boot(): void
    {
        if ($this->app->runningInConsole()) {
            $this->registerMigrations();

            $this->publishes([
                __DIR__.'/../../assets/database/migrations' => database_path('migrations'),
            ], 'hris-saas::ras-migrations');
        }

        $this->registerConfigs();

        Validator::registerValidators();
    }

    /**
     * Register RAS's migration files.
     *
     * @return void
     */
    protected function registerMigrations(): void
    {
        $this->loadMigrationsFrom(__DIR__.'/../../assets/database/migrations');
    }

    /**
     * Register PIM's config files.
     *
     * @return void
     */
    protected function registerConfigs(): void
    {
        $path = realpath(__DIR__.'/../../assets/config/config.php');

        $this->mergeConfigFrom($path, 'hris-saas');
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register(): void
    {
        // TODO: Implement register() method.
    }
}
