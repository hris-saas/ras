<?php

use Illuminate\Support\Facades\Route;
use HRis\PIM\Http\Controllers\StatusController;

// guest
Route::group(['middleware' => 'guest:api'], function () {
    //
});

// auth
Route::group(['middleware' => ['auth:api']], function () {

    // api/approval-statuses
    Route::get('approval-statuses', [StatusController::class, 'index'])->name('approval-status.index');                                                                        // postman
    Route::get('approval-statuses/{approvalStatus}', [StatusController::class, 'show'])->name('approval-status.show');                                                         // postman
    Route::post('approval-statuses', [StatusController::class, 'store'])->name('approval-status.store');                                                                       // postman
    Route::patch('approval-statuses/{approvalStatus}', [StatusController::class, 'update'])->name('approval-status.update');                                                   // postman
    Route::delete('approval-statuses/{approvalStatus}', [StatusController::class, 'destroy'])->name('approval-status.destroy');                                                // postman
    Route::patch('approval-statuses/{approvalStatus}/restore', [StatusController::class, 'restore'])->name('approval-status.restore');                                         // postman
});
