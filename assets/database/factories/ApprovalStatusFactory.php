<?php

namespace Database\Factories;

use HRis\RAS\Eloquent\ApprovalStatus;
use Illuminate\Database\Eloquent\Factories\Factory;

class ApprovalStatusFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = ApprovalStatus::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->name,
        ];
    }
}
