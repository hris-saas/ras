<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use HRis\RAS\Eloquent\ApprovalStatus;

class ApprovalStatusesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('statuses')->where('class', ApprovalStatus::class)->delete();

        DB::table('statuses')->insert([
            [
                'class' => ApprovalStatus::class,
                'name'  => json_encode([
                    'en' => 'Pending',
                    'fr' => 'En attente',
                    'nl' => 'In afwachting van',
                ]),
                'is_completed' => false,
                'sort_order' => 1,
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'class' => ApprovalStatus::class,
                'name'  => json_encode([
                    'en' => 'Approved',
                    'fr' => 'Approuvé',
                    'nl' => 'Goedgekeurd',
                ]),
                'is_completed' => true,
                'sort_order' => 2,
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'class' => ApprovalStatus::class,
                'name'  => json_encode([
                    'en' => 'Cancelled',
                    'fr' => 'Annulé',
                    'nl' => 'Geannuleerd',
                ]),
                'is_completed' => false,
                'sort_order' => 3,
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'class' => ApprovalStatus::class,
                'name'  => json_encode([
                    'en' => 'Denied',
                    'fr' => 'Refusé',
                    'nl' => 'Geweigerd',
                ]),
                'is_completed' => false,
                'sort_order' => 4,
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'class' => ApprovalStatus::class,
                'name'  => json_encode([
                    'en' => 'Deleted',
                    'fr' => 'Supprimé',
                    'nl' => 'Verwijderd',
                ]),
                'is_completed' => false,
                'sort_order' => 5,
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'class' => ApprovalStatus::class,
                'name'  => json_encode([
                    'en' => 'Created',
                    'fr' => 'Créé',
                    'nl' => 'Gemaakt',
                ]),
                'is_completed' => false,
                'sort_order' => 6,
                'created_at' => now(),
                'updated_at' => now(),
            ],
        ]);
    }
}
