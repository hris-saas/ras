<?php

return [
    'database' => [

        'migrations' => [

            'order' => ['core', 'auth', 'ras'],
        ],
    ],

    'permissions' => [
        'ras::approval-status.index',
        'ras::approval-status.store',
        'ras::approval-status.show',
        'ras::approval-status.update',
        'ras::approval-status.destroy',
        'ras::approval-status.restore',
    ],
];
