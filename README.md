## HRis SaaS - Request Approval System

![Coverage](https://gitlab.com/hris-saas/ras/-/raw/develop/coverage.svg)

This software package is part of the HRis SaaS Application.

Request Approval System (RAS) handles and manages the approvals of change request. Starting from time-off requests, personal information change requests, and many more.
